<?php
/**
 * User: Phil Nguyen
 * Date: 12/8/13
 * Time: 11:30 PM
 */


if(isset($_GET['method']) && $_GET['method'] == 'go'){
    if(!isset($_POST['password']) || $_POST['password'] != "bug light 2013"){
        echo "Invalid Password<br>";
    } else {
        if ($_FILES["file"]["error"] > 0)
        {
            echo "Error: " . $_FILES["file"]["error"] . "<br>";
        }
        else
        {
            echo "Upload: " . $_FILES["file"]["name"] . "<br>";
            echo "Type: " . $_FILES["file"]["type"] . "<br>";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
            echo "Stored in: " . $_FILES["file"]["tmp_name"];

            $zip = new ZipArchive;
            $res = $zip->open($_FILES["file"]["tmp_name"]);
            if ($res === TRUE) {
                $zip->extractTo("../");
                echo "<br><b>Success!</b>";
            } else {
                echo "<br><b>Error, could not unzip file</b>";
            }
            $zip->close();
        }
    }

}
?>

<h3>Deploy Application</h3>
<form action="?method=go" method="post"
      enctype="multipart/form-data">
    <label for="file">Filename:</label>
    <input type="file" name="file" id="file"><br>

    <label for="password">Password:</label>
    <input type="password" name="password"><br>

    <input type="submit" name="submit" value="Submit">
</form>