deploy:
	git rev-parse HEAD > race/revision.txt	
	zip -r "race_php-$(shell git rev-parse HEAD)-deploy.zip" race JSON android

clean:
	rm -f race_php-*.zip
