<?php
/**
 * User: Phil Nguyen
 * Date: 11/28/13
 * Time: 12:02 AM
 */

include('header.php');
$heatArr = getHeatSheets();
$transponders = getTransponderColors();
$transponders = array_keys($transponders);

if(array_key_exists(1, $heatArr) && array_key_exists(0, $heatArr[1])){
    header("Location: registeredracers.php");
}

$rounds = array();
$racers = array();
foreach($heatArr as $roundNo=>$roundArr){
    array_push($rounds, trim($roundNo, " \t."));
    foreach($roundArr as $race){
        foreach($race as $racer){
            if(!array_key_exists($racer['DriverID'], $racers)){
                $racers[$racer['DriverID']] = $racer['FullName'];
            }

        }
    }
}
?>

<script>
    $(document).ready(function(){
        $('.table').dataTable({"aaSorting": [[ 0, "asc" ]], "bInfo": false, "bPaginate": false, "bFilter": false});
    });
</script>
    <script>
        $("#nav-raceschedule").addClass('active');
    </script>

<form class="form-inline" role="form">
    <label for="roundFilterSelect">Filter Rounds:</label>
    <select class="form-control" id="roundFilterSelect" style="width: auto;" onchange="javascript:doFilter('round')">
        <option>Show All Rounds</option>
        <? foreach($rounds as $round){ ?>
            <option value="<?echo $round?>">Round <?echo $round?></option>
        <? } ?>
    </select>

    <label for="driverFilterSelect">Filter Drivers:</label>
    <select class="form-control" id="driverFilterSelect" style="width: auto;"  onchange="javascript:doFilter('driver')">
        <option value="all">Show All Drivers</option>
        <? foreach($racers as $driverID => $racer){ ?>
            <option value="<?echo $driverID?>"><?echo $racer?></option>
        <? } ?>
    </select>

    <a href="#" onclick="javascript: clearFilter(); return false">Clear Filter</a>
</form>

<hr>
<?
if(count($heatArr) == 0){
    echo "<div class=\"alert alert-warning\">Sorry, but there are no races posted</div>";
}
foreach($heatArr as $roundNo=>$roundArr){
    $roundNo = trim($roundNo, " \t.");
    echo "<div class=\"panel panel-default\">";

    echo "<div class=\"panel-heading\">
    <h3 class=\"panel-title\">
        <a href=\"#\" onclick=\"javascript:toggleRound($roundNo); return false;\">Round $roundNo</a>
    </h3>
    </div>";
    echo "<div id=\"round_$roundNo\" class=\"roundCollapse\">";
    foreach($roundArr as $raceNo=>$raceArr){
        $isMain = false;
        echo "<div class=\"raceContainer\">";
        echo "<div class=\"panel-body raceHeader\">";
        echo "<h4>Round $roundNo, Race $raceNo - ";

        $classArray = array();

        foreach($raceArr as $classArrayCalc){
            if(!in_array($classArrayCalc['Class'], $classArray)){
                array_push($classArray, $classArrayCalc['Class']);
            }
        }

        echo implode(", ", $classArray);
        if($raceArr[0]['RoundType'] == 'M'){
            $isMain = true;
            echo $raceArr[0]['Heat'] . " Main";
        }

        echo "</h4>";
        echo "</div>";


        echo "<table class=\"table raceTable table-striped table-responsive\">";
        echo "<thead><tr><th class=\"col-sm-1\">#</th><th>Name</th><th>Transponder</th><th>Class</th></tr></thead>";

        echo "<tbody>";
        foreach($raceArr as $racer){
            echo "<tr class=\"driverRow driver_" . $racer['DriverID'] . "\">";
            if($isMain){
                echo "<td>" . $racer['OverallQualPos'] . "</td>";
            } else {
                echo "<td>" . $racer['CarNumber'] . "</td>";
            }


            echo "<td>" . $racer['FullName'] . "</td>";
            //echo "<td>" . ($racer['OverallQualPos'] != 1999 ? $racer['OverallQualPos'] : "Bump") . "</td>";

            if($racer['Transponder'] == ""){
                echo "<td>" . ($raceNo % 2 == 0 ? "Red" : "Black") . " # " . $racer['CarNumber']  . "</td>";
            } else {
                echo "<td>" . $racer['Transponder'] . "</td>";
            }


            echo "<td>" . $racer['Class'] . "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "&nbsp;";


        echo "</div>";


    }
    echo "</div>";
    echo "</div>";
}
?>


<?php include('footer.php'); ?>