<?php include('system/functions.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/jquery.dataTables.css">
    <link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.3.custom.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery-2.0.3.min.js"></script>
    <script  src="js/jquery-ui.custom.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
    <script  src="js/jquery.dataTables.min.js"></script>

    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="flot/excanvas.min.js"></script><![endif]-->
    <script  src="flot/jquery.flot.min.js"></script>
    <script  src="flot/jquery.flot.axislabels.js"></script>
    <script  src="flot/jquery.flot.crosshair.min.js"></script>
    <script  src="flot/jquery.flot.navigate.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Eastridge RC Raceway</title>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61215616-1', 'auto');
  ga('send', 'pageview');

	</script>

    <script>
        jQuery.fx.off = true;
        $(document).ready(function(){

            var currentRoundFilter = localStorage.getItem('roundFilterSelect');
            var currentDriverFilter = localStorage.getItem('driverFilterSelect');

            if(currentRoundFilter != null && $("#roundFilterSelect").length != 0){
                if($("#roundFilterSelect option[value='"+currentRoundFilter+"']").length > 0){
                    $("#roundFilterSelect").val(currentRoundFilter);

                    doFilter('round');
                }

            }
            if(currentDriverFilter != null && $("#driverFilterSelect").length != 0){
                if($("#driverFilterSelect option[value='"+currentDriverFilter+"']").length > 0){
                    $("#driverFilterSelect").val(currentDriverFilter);
                    doFilter('driver');
                }

            }
        });

        function doFilter(type){
            var roundFilter = $('#roundFilterSelect').val();
            var driverFilter = $("#driverFilterSelect").val();

            if(type == "round"){
                if(roundFilter == "Show All Rounds"){
                    $(".roundCollapse").show();
                } else {
                    $('.roundCollapse:not(#round_' + roundFilter + ')').hide(function(){
                        $('#round_' + roundFilter).show();
                    });
                }
                localStorage.setItem("roundFilterSelect", roundFilter);
            } else {
                if(driverFilter == "all"){
                    $('.raceTable .driverRow').show();

                } else {
                    $('.driverRow').hide(function(){
                        $('.driver_' + driverFilter).show();
                    });
                }
                localStorage.setItem('driverFilterSelect', driverFilter);

                $('.raceTable').show();
                $('.raceHeader').show();

                $('.raceTable').each(function(){
                    if( $(this).find('tbody').find("tr:visible").length == 0){
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });

            }
        }

        function clearFilter(){
            $("#roundFilterSelect").val("Show All Rounds");
            doFilter('round')
            $("#driverFilterSelect").val("all");
            doFilter('driver');
        }

        function toggleRound(roundNo){
            $("#round_" + roundNo).toggle();
            doFilter('driver');
        }
    </script>

    <style>
        #footer {
            background-color: #f5f5f5;
            height: 60px;
        }
        .container .credit {
            margin: 20px 0;
        }
    </style>
</head>



<body style="margin-left: 8px; margin-right: 8px">
<h2>Eastridge RC Raceway</h2>

<ul class="nav nav-pills nav-justified">
    <li id="nav-raceschedule"><a href="raceschedule.php">Race Schedule</a></li>
    <li id="nav-raceresults"><a href="raceresults.php">Race Results</a></li>
    <li id="nav-currentrace"><a href="currentrace.php">Current Race</a></li>
    <li id="nav-practiceresults"><a href="practiceresults.php">Practice Results</a></li>
</ul>
<hr>

