<?php
/**
 * User: Phil Nguyen
 * Date: 11/28/13
 * Time: 12:02 AM
 */
header("Location: raceschedule.php");
include('header.php');
?>
    <ul class="nav nav-pills">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="raceschedule.php">Race Schedule</a></li>
        <li><a href="raceresults.php">Race Results</a></li>
    </ul>

<?php include('footer.php'); ?>