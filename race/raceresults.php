<?php
/**
 * User: Phil Nguyen
 * Date: 11/28/13
 * Time: 12:02 AM
 */

include('header.php');
$heatArr = getRaceResults();

$rounds = array();
$racers = array();
foreach($heatArr as $roundNo=>$roundArr){
    array_push($rounds, trim($roundNo, " \t."));
    foreach($roundArr as $race){
        foreach($race as $racer){
            if(!array_key_exists($racer['DriverID'], $racers)){
                $racers[$racer['DriverID']] = $racer['FullName'];
            }

        }
    }
}

?>
<script>
    $(document).ready(function(){
        $('.table').dataTable({"aaSorting": [[ 2, "asc" ]], "bInfo": false, "bPaginate": false, "bFilter": false});
    });
</script>
    <script>
        $("#nav-raceresults").addClass('active');
    </script>
<form class="form-inline" role="form">
    <label for="roundFilterSelect">Filter Rounds:</label>
    <select class="form-control" id="roundFilterSelect" style="width: auto;" onchange="javascript:doFilter('round')">
        <option>Show All Rounds</option>
        <? foreach($rounds as $round){ ?>
            <option value="<?echo $round?>">Round <?echo $round?></option>
        <? } ?>
    </select>

    <label for="driverFilterSelect">Filter Drivers:</label>
    <select class="form-control" id="driverFilterSelect" style="width: auto;"  onchange="javascript:doFilter('driver')">
        <option value="all">Show All Drivers</option>
        <? foreach($racers as $driverID => $racer){ ?>
            <option value="<?echo $driverID?>"><?echo $racer?></option>
        <? } ?>
    </select>

    <a href="#" onclick="javascript: clearFilter(); return false">Clear Filter</a>
</form>

<hr>
<?
if(count($heatArr) == 0){
    echo "<div class=\"alert alert-warning\">Sorry, but there are no races posted</div>";
}
foreach($heatArr as $roundNo=>$roundArr){
    $roundNo = trim($roundNo, " \t.");
    echo "<div class=\"panel panel-default raceDisplay\">";

    echo "<div class=\"panel-heading\">
    <h3 class=\"panel-title\"><a href=\"#\" onclick=\"javascript:toggleRound($roundNo); return false;\">Round $roundNo</a></h3>
    </div>";

    echo "<div id=\"round_$roundNo\" class=\"roundCollapse\">";
    foreach($roundArr as $raceNo=>$raceArr){
        $isMain = false;
        echo "<div class=\"raceContainer\">";
        echo "<div class=\"panel-body raceHeader\">";
        echo "<h4>Round $roundNo, Race $raceNo - ";

        $classArray = array();

        foreach($raceArr as $classArrayCalc){
            if(!in_array($classArrayCalc['Class'], $classArray)){
                array_push($classArray, $classArrayCalc['Class']);
            }
        }

        echo implode(", ", $classArray);
        if($raceArr[0]['RoundType'] == 'M'){
            $isMain = true;
            echo $raceArr[0]['Heat'] . " Main";
        }

        echo "</h4>";
        echo "</div>";

        echo "<table class=\"table raceTable table-striped table-responsive\">";
        echo "<thead>
                <tr>
                <th class=\"col-sm-1\">Car</th>
                <th>Name</th>
                <th class=\"col-sm-1\">Pos</th>
                <th class=\"col-sm-1\">Laps</th>
                <th>Race Time</th>
                <th>Fast Lap</th>
                <th>Top 5</th>
                <th>Top 10</th>
                <th>Qual Pos</th>
                </tr>
              </thead>";

        echo "<tbody>";
        foreach($raceArr as $racer){
            $queryString = http_build_query($racer, "&amp;");
            echo "<tr class=\"driverRow driver_" . $racer['DriverID'] . "\">";
            echo "<td>" . $racer['CarNumber'] . "</td>";
            echo "<td><a href=\"viewpace.php?$queryString\">" . $racer['FullName'] . "</a></td>";
            echo "<td>" . $racer['Position'] . "</td>";
            echo "<td>" . $racer['Laps'] . "</td>";
            echo "<td>" . formatSeconds($racer['RaceTime']) . "</td>";
            echo "<td>" . ($racer['FastLap'] == '999' ? "" : $racer['FastLap']) . "</td>";
            echo "<td>" . ($racer['AveTop5'] == '999' ? "" : $racer['AveTop5']) . "</td>";
            echo "<td>" . ($racer['AveTop10'] == '999' ? "" : $racer['AveTop10']) . "</td>";
            echo "<td>" .
                ($racer['OverallQualPos'] != 1999 ? $racer['OverallQualPos'] : "Bump") .
                " - " . $racer['Class'] .
                "</td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "</table>";
        echo "&nbsp;";
        echo "</div>";
    }
    echo "</div>";
    echo "</div>";
}


?>

<?php include('footer.php'); ?>