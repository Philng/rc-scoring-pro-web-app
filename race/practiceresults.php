<?php
/**
 * User: Phil Nguyen
 * Date: 12/5/13
 * Time: 9:10 PM
 */
include('header.php');

$drivers = getDrivers();
?>

<script>
    var autoRefresh = null;
    $(document).ready(function(){

        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body");

        $("#lapGraph").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(3);
                $("#tooltip").html("Lap " + x + ": " + y)
                    .css({top: item.pageY+5, left: item.pageX+5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });
    });
    function getTransponders(){
        var driverSelect = $("#driverPracticeSelect").val();
        $('#transponderPracticeSelect').find('.transponderEntry').remove();
        $('#transponderPracticeSelect').prop("disabled", "disabled");
        $.post('practiceresults_gettransponder_ajax.php?driverId=' + driverSelect, {}, function(data){
            for(var key in data){
                $('#transponderPracticeSelect')
                    .append($("<option></option>")
                        .attr("value", key )
                        .attr("class", 'transponderEntry')
                        .text(key + ' - ' + data[key]), function(){
                    });
            }
            if(Object.keys(data).length == 1){
                //Select the first value
                $('#transponderPracticeSelect option:eq(1)').attr('selected', 'selected');
                getLaps();
            }

            $('#transponderPracticeSelect').prop("disabled", false);
        }, 'json');
    }

    function getLaps(){
        clearInterval(autoRefresh);
        autoRefreshFunc();
        autoRefresh = setInterval(autoRefreshFunc, 5000);
    }

    function autoRefreshFunc(){
        transponderSelect  = $("#transponderPracticeSelect").val();
        if(transponderSelect != ""){
            $("#autoRefreshID").load('practiceresults_getlaps_ajax.php?transponder=' + transponderSelect);

            $.ajax({
                url : "practiceresults_getlaps_graph_ajax.php?transponder=" + transponderSelect,
                dataType : "json"
            }).done(function(graphData){
                // Set the new graph data
                $("#lapGraph").plot(
                    [ {data : graphData, label : "Lap Time"} ],
                    {
                        series: {
                            lines: {
                                show: true
                            }
                        },
                        grid: {
                            hoverable: true,
                            backgroundColor: { colors: [ "#fff", "#eee" ] },
                        },
                        yaxis: {
                            min: 0
                        },
                        xaxes: [{
                            axisLabel: 'Lap Number',
                            tickDecimals: 0
                        }],
                        yaxes: [{

                            position: 'left',
                            axisLabel: 'Lap Time (Seconds)'
                        }]
                    });
            });

            $(".graphWidget").show();
        } else {
            $("#autoRefreshID").html("");
            $(".graphWidget").hide();
        }
    }
</script>
<script>
    $("#nav-practiceresults").addClass('active');
</script>
<form class="form-inline" role="form">
    <label for="driverPracticeSelect">Filter Drivers:</label>
    <select class="form-control" id="driverPracticeSelect" style="width: auto;" onchange="javascript:getTransponders()">
        <option>Select Driver</option>
        <?foreach($drivers as $driver){?>
        <option value="<?echo $driver['DriverID']?>" class="driverSelect"><?echo $driver['FullName']?></option>

        <? } ?>
    </select>

    <label for="transponderPracticeSelect">Filter Transponders:</label>
    <select class="form-control" id="transponderPracticeSelect" style="width: auto;"  onchange="javascript:getLaps()">
        <option value="">Select Transponder</option>
    </select>

</form>

<hr>

<div class="graphWidget" style="display: none">
    <div class="col-md-8 graphWidget" id="lapGraph" style="width:600px;height:300px;"></div>
    <div class="col-md-1 graphWidget" id="">
    </div>
</div>

<div id="autoRefreshID"></div>

<?php
include('footer.php');
?>