<?php
/**
 * Handles all of the database operations and connections
 *
 * @author Phil Nguyen
 */
class Database {

    /*
     * The connection object
     */
	public $con;
	
	public function __construct(){
		//$this->connect();
	}

    
    /**
     * Connects to the database
     */
    private function connect(){
		$this->con = ads_connect( "DataDirectory=C:\\RC Scoring\\RCSP.add;ServerTypes=1", "", "" ) or die('There was a problem connecting to the database');
    }

    /**
     * Disconnects from the database
     */
    private function disconnect(){
        ads_close($this->con);
    }

	/**
	 * selects and returns an array of results
	 */
	public function select($query, $params = array()){
	   $this->connect();
        $rStmt = ads_prepare($this->con, $query);
        ads_execute( $rStmt, $params );

        $retArray = array();

        $i = 0;
        while(ads_fetch_row($rStmt)){
            $i++;
            $retArray[$i] = ads_fetch_array($rStmt, $i);
        }
        $this->close();

        return $retArray;
	}

    public function close(){
        $this->disconnect();
	   $this->con = null;
    }
}

?>
