<?php
include('advantagedb.php');

date_default_timezone_set('America/New_York');

$todaysDate = date("m/d/Y", time());
$RCSPLocation = "C:\\RC Scoring\\";

//Initialize the database
$db = new Database();


/**
 * Gets the heat sheets in the following fashion:
 *
 * Array[roundNumber][raceNumber]
 * @return array
 */
function getHeatSheets(){
    global $db, $todaysDate;

    $retArray = array();
    $data = $db->select("SELECT * FROM entries WHERE date = ? ORDER BY Round, RaceNo, OverallQualPos ASC", array($todaysDate));

    foreach($data as $row){
       $retArray[trim($row['Round'], " \t.")][trim($row['RaceNo'], " \t.")] = array();
    }
    foreach($data as $row){
        array_push($retArray[trim($row['Round'], " \t.")][trim($row['RaceNo'], " \t.")], $row);
    }

    return $retArray;
}


/**
 * Gets the race result in the following fashion:
 *
 * Array[roundNumber][raceNumber]
 * @return array
 */
function getRaceResults(){
    global $db, $todaysDate;

    $retArray = array();
    $data = $db->select("SELECT * FROM entries WHERE date = ? AND RaceLengthSeconds IS NOT NULL ORDER BY Round, RaceNo, Position ASC", array($todaysDate));

    foreach($data as $row){
        $retArray[trim($row['Round'], " \t.")][trim($row['RaceNo'], " \t.")] = array();
    }
    foreach($data as $row){
        array_push($retArray[trim($row['Round'], " \t.")][trim($row['RaceNo'], " \t.")], $row);
    }

    return $retArray;
}

function getPaceInfo($driverID, $roundNumber, $raceNumber){
    global $db, $todaysDate;

    $query = "
        SELECT * FROM practice
        WHERE date = ?
          AND DriverID = ?
          AND RaceNo = ?
          AND Round = ?
          AND Laps <> 0
          AND ClassId <> -1
        ORDER BY Laps ASC
    ";
    $queryOptions = array($todaysDate, $driverID, $raceNumber, $roundNumber);

    $data = $db->select($query, $queryOptions);

    return $data;
}

/**
 * Format seconds to mins:seconds.milliseconds
 */
function formatSeconds( $seconds ){
    $hours = 0;
    $milliseconds = str_replace( "0.", '', round($seconds - floor( $seconds ), 3) );;
    return gmdate( 'i:s', $seconds ) . ($milliseconds ? ".$milliseconds" : '');
}

/**
 * Gets a current race
 */
function getCurrentRace(){
    global $db, $todaysDate;

    $query = "SELECT RaceNo, Round, RoundType, RaceLengthSeconds, Heat, Class FROM Entries
    WHERE date = ? AND RaceLengthSeconds IS NULL ORDER BY Round ASC, RaceNo ASC;";
    $queryOptions = array($todaysDate);

    $data = $db->select($query, $queryOptions);

    if(array_key_exists(1, $data)){
        return $data[1];
    } else {
        return -1;
    }

}

/**
 * Gets the XML data file from RC Scoring Pro
 * @return SimpleXMLElement
 */
function getLiveRaceXML(){
    global $RCSPLocation;
    return simplexml_load_file($RCSPLocation . "driverData.xml");
}

/**
 * Gets the overall race information from the xml file
 * @return mixed
 */
function getLiveRaceData(){
    return getLiveRaceXML()->RaceData->Race->attributes();
}

/**
 * Gets each driver's information during the race
 * @return mixed
 */
function getLiveRaceDriverData(){
    return getLiveRaceXML()->DriverData->Driver;
}

/**
 * Gets an array of drivers in the database that were here today
 */
function getDrivers(){
    global $db, $todaysDate;
    $query = "SELECT DISTINCT driver.DriverID, driver.FullName
    FROM driver
    JOIN practice ON driver.DriverID = practice.DriverID AND practice.LapType = 0
    ORDER BY FullName ASC";
    return $db->select($query, array($todaysDate));

}

function getTransponder($driverId){
    global $db, $todaysDate;
    $query = "SELECT practice.*, Classes.Class FROM practice JOIN Classes ON practice.ClassID = Classes.ClassID
     WHERE DriverID = ? AND Date = ?";
    $data = $db->select($query, array($driverId, $todaysDate));

    $returnArray = array();
    foreach($data as $driver){
        $returnArray[$driver['Transponder']] = $driver['Class'];
    }
    return $returnArray;
}

function getLapsForTransponder($transponder){
    global $db, $todaysDate;
    $query = "SELECT * FROM practice JOIN Classes ON practice.ClassID = Classes.ClassID
    WHERE Transponder = ? AND Date = ? AND LapType = 0 ORDER BY LapCounter DESC;";

    return $db->select($query, array($transponder, $todaysDate));
}

/**
 * @param $data Data from getRaceSchedule();
 * @return array Returns array[key] = array() where key is driver name and array() is classes
 * which the driver is registered in
 */
function formatRegisteredRacers($data){
    $returnArr = array();
    foreach($data as $entry){
        if(!array_key_exists($entry['FullName'], $returnArr)){
            $returnArr[$entry['FullName']] = array();
        }

        array_push($returnArr[$entry['FullName']], $entry['Class']);
    }

    return $returnArr;
}

/**
 * Sort a 2D Array
 * @param $array
 * @param $key
 */
function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
}

function isDebug(){
    global $_COOKIE;
    if(isset($_COOKIE['XDEBUG_SESSION'])){
        return true;
    } else {
        return false;
    }

}

function getTransponderColors(){
    global $db;

    $query = "SELECT * FROM loanertransponders";
    $data = $db->select($query, array());

    $returnArr = array();
    foreach($data as $transponder){
        if(!array_key_exists($transponder['Color'], $returnArr)){
            $returnArr[$transponder['Color']] = array();
        }
        array_push($returnArr[$transponder['Color']], $transponder['CarNumber']);
    }

    return $returnArr;
}
?>
