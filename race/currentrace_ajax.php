<?php
/**
 * User: Phil Nguyen
 * Date: 11/30/13
 * Time: 6:41 PM
 */
include('system/functions.php');

$currentRaceData = getLiveRaceData();
?>
    <div class="jumbotron">
        <? if((int)$currentRaceData->raceNumber <= (int)$currentRaceData->totalRaces){?>
        <h3>Round <?echo $currentRaceData->round ?>, Race <?echo $currentRaceData->raceNumber ?> / <?echo $currentRaceData->totalRaces ?> </h3>
        <? } else { ?>
            <h3>Waiting for next round</h3>
        <? } ?>
        <p><b>Class:</b> <?echo $currentRaceData->className ?></p>
        <p><b>Top Qualifier:</b> <?echo ($currentRaceData->tq == "(null)" ? "" : $currentRaceData->tq)?></p>
        <p><b>Race Status:</b>
            <?
            if($currentRaceData->raceStatus == "running" && $currentRaceData->timeRemaining > 0) {
                echo "<span class=\"label label-success\">Running</span>";
            } else if($currentRaceData->raceStatus == "Staging"){
                echo "<span class=\"label label-warning\">Staging</span>";
            } else {
                echo "<span class=\"label label-default\">Done</span>";
            }
            ?>
        </p>
        <!--<p><b>Time Remaining:</b>
            <?echo ($currentRaceData->timeRemaining > 0 ? gmdate("i:s", (int)$currentRaceData->timeRemaining) : "0:00") ?>
        </p>-->

        <table class="table table-striped table-condensed table-responsive">
            <thead>
            <th>Pos</th>
            <th>Driver Name</th>
            <th>Laps</th>
            <th>Lap Time</th>
            <th>Pace</th>
            <th>Fast Lap</th>
            <th>Behind</th>
            </thead>
            <tbody>
            <?foreach(getLiveRaceDriverData() as $raceData){ ?>
                <tr>
                    <td><? echo $raceData->attributes()->position ?></td>
                    <td>
                        <? echo $raceData->attributes()->name ?>
                        <?
                        if($raceData->attributes()->done == 'true') {
                            echo "<span class=\"label label-warning\">Done</span>";
                        } else if($raceData->attributes()->CheckedIn == 1 && $currentRaceData->raceStatus == "running"){
                            echo "<span class=\"label label-success\">Racing</span>";
                        } else if($raceData->attributes()->CheckedIn == '1'){
                            echo "<span class=\"label label-primary\">Checked In</span>";
                        }
                        ?>
                    </td>
                    <td><? echo $raceData->attributes()->laps ?></td>
                    <td><? echo $raceData->attributes()->lapTime ?></td>
                    <td><? echo $raceData->attributes()->pace ?></td>
                    <td><? echo $raceData->attributes()->fastLap ?></td>
                    <td><? echo $raceData->attributes()->difference ?></td>
                </tr>
            <? } ?>
            </tbody>
        </table>

    </div>
