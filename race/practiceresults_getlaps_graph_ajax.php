<?php
/**
 * User: Phil Nguyen
 * Date: 12/5/13
 * Time: 11:43 PM
 */
include('system/functions.php');

$data = getLapsForTransponder($_GET['transponder']);
$reversed = array_reverse($data);

$returnArray = array();
for($i = 0; $i < count($reversed); $i++){
    if($reversed[$i]['Difference'] != ""){
        array_push($returnArray, array($i, $reversed[$i]['Difference']));
    }
}

echo json_encode($returnArray);
?>
