<?php
/**
 * User: Phil Nguyen
 * Date: 12/5/13
 * Time: 11:43 PM
 */
include('system/functions.php');

$data = getLapsForTransponder($_GET['transponder']);

?>
<script>
    $(document).ready(function(){
        $('.table').dataTable({"aaSorting": [[ 2, "desc" ]], "bInfo": false, "bPaginate": false, "bFilter": false});
    });
</script>

<div style="width: 30%; min-width: 300px;">
    <table class="table table-striped table-responsive">
        <thead>
            <th>Laps</th>
            <th>Lap Time</th>
            <th>Time</th>
        </thead>
        <?foreach($data as $dat){ ?>
            <?if($dat['Laps'] != ""){ ?>
            <tr><td><?echo $dat['Laps']?></td><td><?echo $dat["Difference"]?></td><td><?echo $dat['Time']?></td></tr>
            <? } ?>
        <? } ?>

    </table>
</div>

