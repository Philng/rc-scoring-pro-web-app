<?php
/**
 * User: Phil Nguyen
 * Date: 12/12/13
 * Time: 5:01 PM
 */

include('system/functions.php');

$currentRaceData = getLiveRaceData();
$driverRaceData = getLiveRaceDriverData();

$returnArray = array(
    'raceNo' => (int) $currentRaceData->raceNumber,
    'roundNo' => (int )$currentRaceData->round,
    'timeElapsed' => (int) $currentRaceData->timeElapsed,
    'raceStatus' => (string) $currentRaceData->raceStatus,
    'driverData' => array());

foreach($driverRaceData as $driverData){
    $data = $driverData->attributes();
    $driverArray = array(
        'driverName' => (string) $data['name'],
        'position'  => (int) $data['position'],
        'timeElapsed' => (int) $currentRaceData->timeElapsed,
        'laps' => (int) $data['laps'],
        'lapTime' => (string) $data['lapTime'],
        'pace' => (int) substr($data['pace'], 0, strpos($data['pace'], "/"))

    );

    array_push($returnArray['driverData'], $driverArray);
}

echo json_encode($returnArray);

?>