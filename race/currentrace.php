<?php
/**
 * User: Phil Nguyen
 * Date: 11/30/13
 * Time: 6:41 PM
 */
include('header.php');

$currentRaceData = getLiveRaceData();
?>
<script>
    $("#nav-currentrace").addClass('active');
</script>

<script>
    var positionGraphData = [];
    var lapTimeGraphData = [];
    var paceGraphData = []
    var raceNo = 0;
    var round = 0;
    var timeElapsed = 0;

    $(function() {
        startRefresh();

        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body");

        $("#currentLapTimeGraph, #currentPaceGraph").bind("plothover", function (event, pos, item) {
            if (item) {
                if($(this).attr('id') == "currentPaceGraph"){
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(0);
                    z = item.series.label;

                    $("#tooltip").html(z + "'s Pace: " + y + " laps")
                        .css({top: item.pageY+5, left: item.pageX+5})
                        .fadeIn(200);
                } else {
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(3);
                    z = item.series.label;

                    $("#tooltip").html(z + " Lap " + x + ": " + y)
                        .css({top: item.pageY+5, left: item.pageX+5})
                        .fadeIn(200);
                }
            } else {
                $("#tooltip").hide();
            }
        });
    });

    function startRefresh() {
        updateGraphData(doAutoRefresh);
        $.get('currentrace_ajax.php', function(data) {
            $('#currentRaceDiv').html(data);
        });

    }

    function doAutoRefresh(){
        setTimeout(startRefresh, 5000);
    }

    function updateGraphData(callback){
        $.ajax({
            url : "currentrace_graph_ajax.php",
            dataType : "json",
            async:   false
        }).done(function(jsonData){
            if(jsonData.raceNo != raceNo || jsonData.round != round || jsonData.timeElapsed < timeElapsed){
                //Reset the graph series if this is a new round
                positionGraphData = [];
                lapTimeGraphData = [];
                paceGraphData = [];
                raceNo = jsonData.raceNo;
                round = jsonData.round;
                $("#positionSelectContainer").html('');
                $("#lapSelectContainer").html('');
                $("#paceSelectContainer").html('');
            }
            timeElapsed = jsonData.timeElapsed;

            if(jsonData.raceStatus == "running"){
                var driverData = jsonData.driverData;
                for(var i = 0; i < driverData.length; i++){

                    //Populate Position Graph
                    var arrayPos = lookup(positionGraphData, driverData[i].driverName );
                    if(arrayPos === -1){
                        //Not found, make it
                        var newObj = {data: [driverData[i].timeElapsed, driverData[i].position], label: driverData[i].driverName };
                        positionGraphData.push(newObj);

                        var choiceContainer = $("#positionSelectContainer");
                        choiceContainer.append("<br/><input onclick='plotLapGraph()' type='checkbox' name='" + driverData[i].driverName +
                            "' checked='checked' id='positionid" + driverData[i].driverName + "'></input>" +
                            "<label for='id" + driverData[i].driverName  + "'>&nbsp;"
                            + driverData[i].driverName  + "</label>");
                    } else {
                        //Found, push new data onto it
                        positionGraphData[arrayPos].data.push( [driverData[i].timeElapsed, driverData[i].position] );
                    }

                    //Populate Lap time graph
                    var arrayPos = lookup(lapTimeGraphData, driverData[i].driverName );
                    if(arrayPos === -1){
                        //Not found, make it
                        var newObj2 = {data: [driverData[i].laps, driverData[i].lapTime], label : driverData[i].driverName };
                        lapTimeGraphData.push(newObj2);

                        var choiceContainer = $("#lapSelectContainer");
                        choiceContainer.append("<br/><input onclick='plotLapTimeGraph()' type='checkbox' name='" + driverData[i].driverName +
                            "' checked='checked' id='lapid" + driverData[i].driverName + "'></input>" +
                            "<label for='id" + driverData[i].driverName  + "'>&nbsp;"
                            + driverData[i].driverName  + "</label>");
                    } else {
                        //Look to see if the lap number is already in the array
                        lapTimeGraphData[arrayPos].data.push( [driverData[i].laps, driverData[i].lapTime] );
                    }

                    //Pace graph
                    var arrayPos = lookup(paceGraphData, driverData[i].driverName );
                    if(arrayPos === -1){
                        //Not found, make it
                        var newObj3 = {data: [driverData[i].laps, driverData[i].pace], label : driverData[i].driverName };
                        paceGraphData.push(newObj3);

                        var choiceContainer = $("#paceSelectContainer");
                        choiceContainer.append("<br/><input onclick='plotPaceGraph()' type='checkbox' name='" + driverData[i].driverName +
                            "' checked='checked' id='lapid" + driverData[i].driverName + "'></input>" +
                            "<label for='id" + driverData[i].driverName  + "'>&nbsp;"
                            + driverData[i].driverName  + "</label>");
                    } else {
                        //Look to see if the lap number is already in the array
                        paceGraphData[arrayPos].data.push( [driverData[i].laps, driverData[i].pace] );
                    }
                }
            }

            plotLapGraph();
            plotLapTimeGraph();
            plotPaceGraph();
            callback();
        });
    }

    function plotPaceGraph(){
        filteredGraphData = new Array();

        // hard-code color indices to prevent them from shifting as
        // countries are turned on/off

        var i = 0;
        $.each(paceGraphData, function(key, val) {
            val.color = i;
            ++i;
        });

        var choiceContainer = $("#paceSelectContainer");
        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            lookupResult = lookup(paceGraphData, key);
            if (lookupResult != -1) {
                filteredGraphData.push(paceGraphData[lookupResult]);
            }
        });

        var plot = $.plot(
            "#currentPaceGraph",
            filteredGraphData,
            {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true,
                    backgroundColor: { colors: [ "#fff", "#eee" ] }
                },
                yaxis: {
                    tickDecimals: 0,
                    axisLabel: 'Pace',
                    panRange: [1, 8]
                },
                xaxis: {
                    tickDecimals:0,
                    axisLabel: 'Lap',
                    panRange: [0, 1500]
                },
                legend: {
                    position : "nw"
                }
            });


    }
    function plotLapTimeGraph(){
        filteredGraphData = new Array();

        // hard-code color indices to prevent them from shifting as
        // countries are turned on/off

        var i = 0;
        $.each(lapTimeGraphData, function(key, val) {
            val.color = i;
            ++i;
        });

        var choiceContainer = $("#lapSelectContainer");
        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            lookupResult = lookup(lapTimeGraphData, key);
            if (lookupResult != -1) {
                filteredGraphData.push(lapTimeGraphData[lookupResult]);
            }
        });

        var plot = $.plot(
            "#currentLapTimeGraph",
            filteredGraphData,
            {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true,
                    backgroundColor: { colors: [ "#fff", "#eee" ] }
                },
                yaxis: {
                    axisLabel: 'Lap Time',
                    panRange: [1, 8]
                },
                xaxis: {
                    min: 0,
                    tickDecimals:0,
                    axisLabel: 'Lap',
                    panRange: [0, 1500]
                },
                legend: {
                    position : "nw"
                }
            });


    }
    function plotLapGraph(){
        filteredGraphData = new Array();

        // hard-code color indices to prevent them from shifting as
        // countries are turned on/off

        var i = 0;
        $.each(positionGraphData, function(key, val) {
            val.color = i;
            ++i;
        });

        var choiceContainer = $("#positionSelectContainer");
        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            lookupResult = lookup(positionGraphData, key);
            if (lookupResult != -1) {
                filteredGraphData.push(positionGraphData[lookupResult]);
            }
        });

        var plot = $.plot(
            "#currentRaceGraph",
            filteredGraphData,
            {
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true
                }
            },
            grid: {
                hoverable: true,
                backgroundColor: { colors: [ "#fff", "#eee" ] }
            },
            yaxis: {
                min: 1,
                tickDecimals:0,
                axisLabel: 'Position',
                transform: function (v) { return -v; },
                inverseTransform: function (v) { return -v; },
                panRange: [1, 8]
            },
            xaxis: {
                tickDecimals:0,
                axisLabel: 'Time Elapsed (Seconds)',
                panRange: [0, 1500]
            },
            legend: {
                position : "nw"
            }
        });

    }

    function lookup(graphData, name ) {
        for(var i = 0, len = graphData.length; i < len; i++) {
            if( graphData[ i ].label === name )
                return i;
        }
        return -1;
    }

</script>
<div id="currentRaceDiv"></div>

<h3>Position Graph</h3>
<div class="well" style="">
    <div class="col-md-7" id="currentRaceGraph" style="width: 80%; height: 300px;"></div>
    <div class="col-md-2" id="positionSelectContainer"></div>
    <div style="clear: both"></div>
</div>

<h3>Lap Time Graph</h3>
<div class="well" style="">
    <div class="col-md-7" id="currentLapTimeGraph" style="width: 80%; height: 300px;"></div>
    <div class="col-md-2" id="lapSelectContainer"></div>
    <div style="clear: both"></div>
</div>

    <h3>Pace Graph</h3>
    <div class="well" style="">
        <div class="col-md-7" id="currentPaceGraph" style="width: 80%; height: 300px;"></div>
        <div class="col-md-2" id="paceSelectContainer"></div>
        <div style="clear: both"></div>
    </div>
<?php
include('footer.php');
?>