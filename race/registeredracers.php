<?php
/**
 * User: Phil Nguyen
 * Date: 12/3/13
 * Time: 4:34 PM
 */
include('header.php');

$data = getHeatSheets();

if(!array_key_exists(1, $data) || !array_key_exists(0, $data[1])){
    header("Location: raceschedule.php");
}
$data = formatRegisteredRacers($data[1][0]);



?>
<script>
    $("#nav-raceschedule").addClass('active');
</script>
<h3>Registered Racers</h3>
<table class="table">
    <thead>
        <th>Name</th>
        <th>Class</th>
    </thead>
    <tbody>
    <?php foreach($data as $racer=>$classes){ ?>
        <tr>
            <td><?echo $racer ?></td>
            <td><?echo implode(", ", $classes) ?></td>
        </tr>

    <? } ?>

    </tbody>
</table>