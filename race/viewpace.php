<?php
/**
 * User: Phil Nguyen
 * Date: 11/29/13
 * Time: 7:59 PM
 */
include('header.php');
$paceInfo = getPaceInfo($_GET['DriverID'], $_GET['Round'], $_GET['RaceNo']);

$LapTimeJSON = array();
$paceJSON = array();
foreach($paceInfo as $pace){
    array_push($LapTimeJSON, array($pace['Laps'], $pace['Difference']));
    array_push($paceJSON, array($pace['Laps'], substr($pace['Pace'], 0, strpos($pace['Pace'], "/"))));
}
?>
<script>
    $(document).ready(function(){
        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body");

        $("#lapGraph").bind("plothover", function (event, pos, item) {
            if (item) {
                console.log(item);
                var x = item.datapoint[0].toFixed(0);

                if(item.series.label == "Lap Time"){
                    var y = item.datapoint[1].toFixed(3);
                    $("#tooltip").html("Lap " + x + ": " + y)
                        .css({top: item.pageY+5, left: item.pageX+5})
                        .fadeIn(200);
                } else {
                    var y = item.datapoint[1].toFixed(0);
                    $("#tooltip").html("Lap " + x + " pace: " + y + " laps")
                        .css({top: item.pageY+5, left: item.pageX+5})
                        .fadeIn(200);
                }
            } else {
                $("#tooltip").hide();
            }
        });

        $('.table').dataTable({"aaSorting": [[ 0, "asc" ]], "bInfo": false, "bPaginate": false, "bFilter": false});

        var lapTimeObj = <? echo json_encode($LapTimeJSON) ?>;
        var paceObj = <? echo json_encode($paceJSON) ?>;
        var plot = $("#lapGraph").plot(
            [ {data : lapTimeObj, label : "Lap Time"}, {data : paceObj, label : "Pace", yaxis : 2} ],
            {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true,
                    backgroundColor: { colors: [ "#fff", "#eee" ] }
                },
                xaxes: [{
                    axisLabel: 'Lap Number',
                    tickSize:1,
                    tickDecimals:0
                }],
                yaxes: [{
                        position: 'left',
                        axisLabel: 'Lap Time (Seconds)',
                        min: 0
                    },
                    {   position: 'right',
                        axisLabel: "Laps",
                        tickDecimals:0
                    }
                ]
            });
    });
</script>
    <script>
        $("#nav-raceresults").addClass('active');
    </script>
<hr>
    <ol class="breadcrumb">
        <li><a href="raceresults.php">Race Results</a></li>
        <li class="active">Pace Information</li>
    </ol>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Pace Info for <?php echo $_GET['FullName']; ?></h3>
    </div>

    <div class="panel-body">
        <p>Round <?php echo $_GET['Round'] ?>, Race <?php echo $_GET['RaceNo'] ?> -
            <?
            echo $_GET['Class'];
            if($_GET['RoundType'] == 'M'){
                $isMain = true;
                echo $_GET['Heat'] . " Main";
            }
            ?>
        </p>
    </div>

    <div class="graphWidget" style="">
        <div class="col-md-8 graphWidget" id="lapGraph" style="width:550px;height:300px;"></div>
        <div class="col-md-1 graphWidget" id=""></div>
    </div>

    <div style="width: 550px; min-width: 300px;">
        <table class="table table-striped">
            <thead>
                <tr>
                <th class="col-sm-2">Lap #</th>
                <th>Pace</th>
                <th>Lap Time</th>
                </tr>
            </thead>
            <tbody>
            <?foreach($paceInfo as $pace){?>
                <tr>
                    <td><?php echo $pace['Laps']?></td>
                    <td><?php echo $pace['Pace']?></td>
                    <td><?php echo $pace['Difference']?></td>
                </tr>
            <? } ?>
            </tbody>

        </table>
    </div>

</div>



<? include('footer.php'); ?>