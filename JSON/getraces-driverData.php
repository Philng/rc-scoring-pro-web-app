<?
include("../race/system/functions.php");


$paceInfo = getPaceInfo($_GET['DriverID'], $_GET['Round'], $_GET['RaceNo']);

$LapTimeJSON = array();
$paceJSON = array();
foreach($paceInfo as $pace){
    array_push($LapTimeJSON, array($pace['Laps'], $pace['Difference']));
    array_push($paceJSON, array($pace['Laps'], $pace['Pace']));
}


$returnArray = array("LapTime" => $LapTimeJSON, "Pace" => $paceJSON);

echo json_encode($returnArray);
?>