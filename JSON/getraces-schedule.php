<?
include("../race/system/functions.php");

$roundNo = $_GET['round'];

$data = getHeatSheets();
$data = $data[$roundNo];


$json_array = array();

foreach($data as $RaceNoKey => $race){

	$className = ($race[0]['RoundType'] == "M" ? rtrim($race[0]['Class']) . " - " . rtrim($race[0]['Heat']) . " Main" : rtrim($race[0]['Class']));

	//Apply House Transponders if needed
	foreach($race as &$driver){
	    if($driver['Transponder'] == ""){
            $driver['Transponder'] = ($RaceNoKey % 2 == 0 ? "Red" : "Black") . " # " . $driver['CarNumber'];
        }
	}
	
	$newArray = array(
		"RaceNo" 	=> $RaceNoKey,
		"Class"		=> $className,
		"Drivers"	=> $race
	);
	
	
	array_push($json_array, $newArray);
}
echo json_encode($json_array);
?>