<?

include('../race/system/functions.php');

$data = $db->select("SELECT DISTINCT Round FROM entries WHERE date = ? AND RaceLengthSeconds IS NOT NULL ORDER BY Round, RaceNo, Position ASC", array($todaysDate));

$jsonReturn = array();

foreach($data as $dat){
	$strStrp = str_replace(" ", "", $dat['Round']);
	array_push($jsonReturn, $strStrp);
}

echo json_encode($jsonReturn);

?>