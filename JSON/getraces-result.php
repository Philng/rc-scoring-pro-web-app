<?
include("../race/system/functions.php");

$roundNo = $_GET['round'];

$data = getRaceResults();
$data = $data[$roundNo];


$json_array = array();

foreach($data as $RaceNoKey => $race){

	$className = ($race[0]['RoundType'] == "M" ? rtrim($race[0]['Class']) . " - " . rtrim($race[0]['Heat']) . " Main" : rtrim($race[0]['Class']));

	foreach($race as &$driver){
		
		//Clean trailing spaces
		foreach($driver as &$driverData){
			$driverData = rtrim($driverData);
		}
		
		$driver['RaceTime'] = formatSeconds($driver['RaceTime']);
		$driver['FastLap'] = ($driver['FastLap'] == '999' ? "" : strval(round($driver['FastLap'], 3)));
	}
	
	$newArray = array(
		"RaceNo" 	=> $RaceNoKey,
		"Class"		=> $className,
		"Drivers"	=> $race
	);             
	
	array_push($json_array, $newArray);
}

echo json_encode($json_array);
?>