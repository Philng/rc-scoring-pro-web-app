<?
include('../race/system/functions.php');


$array =  getLiveRaceXML();

if($array->RaceData->Race->attributes()->raceStatus == "running" && $array->RaceData->Race->attributes()->timeRemaining > 0){
	$array->RaceData->Race->attributes()->raceStatus = "Running";
} else if($array->RaceData->Race->attributes()->raceStatus == "Staging"){
	$array->RaceData->Race->attributes()->raceStatus = "Staging";
} else {
	$array->RaceData->Race->attributes()->raceStatus = "Done";
}
echo json_encode($array);
