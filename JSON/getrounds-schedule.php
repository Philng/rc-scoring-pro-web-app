<?php
include('../race/system/functions.php');

$data = $db->select("SELECT DISTINCT Round FROM entries WHERE date = ?", array($todaysDate));

$jsonReturn = array();

foreach($data as $dat){
	$strStrp = str_replace(" ", "", $dat['Round']);
	array_push($jsonReturn, $strStrp);
}

echo json_encode($jsonReturn);
?>