<?php
/**
 * User: Phil Nguyen
 * Date: 1/22/14
 * Time: 1:36 PM
 */
include("../race/system/functions.php");
include('header.php');

$heatArr = getHeatSheets();
?>
<script>
    $(document).ready(function(){
        $("#navbar .schedule").addClass("ui-btn-active");
    });
</script>

<h1>Schedule</h1>


<ul data-role="listview" data-inset="true">
<?
foreach($heatArr as $roundNo=>$roundArr){
    $roundNo = trim($roundNo, " \t.");

    echo "<li><a href=\"schedule-races.php?roundNo=$roundNo\">Round $roundNo</a></li>";
}
?>
</ul>

<?php
include('footer.php');
?>