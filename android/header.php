<!DOCTYPE html>
<html>
<head>
    <title></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
    <meta name="HandheldFriendly" content="true" />

    <link rel="stylesheet" href="jquerymobile/jquery.mobile-1.4.0.min.css" />
    <script src="jquerymobile/jquery-1.10.2.min.js"></script>
    <script src="jquerymobile/jquery.mobile-1.4.0.min.js"></script>

</head>
<body>
<script>
    $.mobile.page.prototype.options.domCache = false;
    function refreshPage() {
        $.mobile.changePage(
            window.location.href,
            {
                allowSamePageTransition : true,
                transition              : 'none',
                showLoadMsg             : true,
                reloadPage              : true
            }
        );
    }


</script>
<div data-role="header">
    <h1>Eastridge RC Raceway</h1>
    <a href="#" onclick="javascript:refreshPage()" class="ui-btn-right" id="pageRefresh" data-icon="refresh">Refresh</a>
</div>

<div data-role="navbar" id="navbar">
    <ul>
        <li><a class="schedule" href="schedule.php">Schedule</a></li>
        <li><a class="results" href="results.php">Results</a></li>
        <li><a class="live" href="live.php">Live</a></li>
    </ul>
</div><!-- /navbar -->