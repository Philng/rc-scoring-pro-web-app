<?php
/**
 * User: Phil Nguyen
 * Date: 1/22/14
 * Time: 3:15 PM
 */
include("../race/system/functions.php");
include('header.php');

$currentRaceData = getLiveRaceData();
?>
<script>

    $(document).ready(function(){
        $("#navbar .live").addClass("ui-btn-active");
        setTimeout(refreshPage, 5000);
    });

    function refreshPage()
    {
        jQuery.mobile.changePage(window.location.href, {
            allowSamePageTransition: true,
            transition: 'none',
            reloadPage: true
        });
    }

</script>
<h1>Live</h1>
<div class="ui-grid-a">
    <div class="ui-block-a"><div class="ui-body">
            <table style="">
                <tr><td style="padding-right: 20px;"><b>Status:</b></td><td>
                        <?
                        if($currentRaceData->raceStatus == "running" && $currentRaceData->timeRemaining > 0) {
                            echo "Running";
                        } else if($currentRaceData->raceStatus == "Staging"){
                            echo "Staging";
                        } else {
                            echo "Done";
                        }
                        ?>

                    </td></tr>
                <tr><td style=""><b>Race:</b></td><td>
                        <? if((int)$currentRaceData->raceNumber <= (int)$currentRaceData->totalRaces){?>
                            Round <?echo $currentRaceData->round ?>, Race <?echo $currentRaceData->raceNumber ?> / <?echo $currentRaceData->totalRaces ?>
                        <? } else { ?>
                            Waiting for next round
                        <? } ?>

                    </td></tr>
                <tr><td style=""><b>Class:</b></td><td><?echo $currentRaceData->className ?></td></tr>
            </table><br>
        </div></div>
</div><!-- /grid-a -->

<ul data-role="listview">
    <?foreach(getLiveRaceDriverData() as $raceData){ ?>
        <li>
            <div>
            <b><?=$raceData->attributes()->name?></b>
            <br>
            <b>Laps:</b> <? echo $raceData->attributes()->laps ?> |
            <b>Lap Time:</b> <? echo $raceData->attributes()->lapTime ?> |
            <b>Pace:</b> <? echo $raceData->attributes()->pace ?> |
            <b>Fast Lap:</b> <? echo $raceData->attributes()->fastLap ?> |
            <b>Difference:</b> <? echo $raceData->attributes()->difference ?>
            </div>
        </li>

    <?}?>

</ul>