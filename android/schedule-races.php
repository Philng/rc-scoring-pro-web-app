<?php
/**
 * User: Phil Nguyen
 * Date: 1/22/14
 * Time: 1:46 PM
 */
if(!isset($_GET['roundNo'])){
    header("Location: schedule.php");
}
include("../race/system/functions.php");
include('header.php');

$roundNo = $_GET['roundNo'];

$data = getHeatSheets();
$data = $data[$roundNo];

$raceArray = array();

foreach($data as $RaceNoKey => $race){

    $className = ($race[0]['RoundType'] == "M" ? rtrim($race[0]['Class']) . " - " . rtrim($race[0]['Heat']) . " Main" : rtrim($race[0]['Class']));

    //Apply House Transponders if needed
    foreach($race as &$driver){
        if($driver['Transponder'] == ""){
            $driver['Transponder'] = ($RaceNoKey % 2 == 0 ? "Red" : "Black") . " # " . $driver['CarNumber'];
        }
    }

    $newArray = array(
        "RaceNo" 	=> $RaceNoKey,
        "Class"		=> $className,
        "Drivers"	=> $race
    );
    array_push($raceArray, $newArray);
}
?>

<script>
    $(document).ready(function(){
        $("#navbar .schedule").addClass("ui-btn-active");
    });
</script>
<h1>Races For Round <?=$roundNo?></h1>
<ul data-role="listview" data-inset="true">
    <?foreach($raceArray as $race){
        echo "<li data-role=\"list-divider\">Race ".$race['RaceNo']." - ".$race['Class']."</li>";

        foreach($race['Drivers'] as $driver){
            echo "<li>".$driver['FullName']."<p class=\"ui-li-aside\">".$driver['Transponder']."</p></li>";
        }

    }?>


</ul>