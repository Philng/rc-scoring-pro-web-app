<?php
/**
 * User: Phil Nguyen
 * Date: 1/22/14
 * Time: 1:36 PM
 */
include("../race/system/functions.php");
include('header.php');

$data = $db->select("SELECT DISTINCT Round FROM entries WHERE date = ? AND RaceLengthSeconds IS NOT NULL ORDER BY Round, RaceNo, Position ASC", array($todaysDate));

$jsonReturn = array();

foreach($data as $dat){
    $strStrp = str_replace(" ", "", $dat['Round']);
    array_push($jsonReturn, $strStrp);
}

?>
<script>
    $(document).ready(function(){
        $("#navbar .results").addClass("ui-btn-active");
    });
</script>

<h1>Results</h1>


<ul data-role="listview" data-inset="true">
    <?
    foreach($jsonReturn as $roundNo){
        $roundNo = trim($roundNo, " \t.");

        echo "<li><a href=\"results-races.php?roundNo=$roundNo\">Round $roundNo</a></li>";
    }
    ?>
</ul>

<?php
include('footer.php');
?>