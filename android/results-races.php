<?php
/**
 * User: Phil Nguyen
 * Date: 1/22/14
 * Time: 2:12 PM
 */

if(!isset($_GET['roundNo'])){
    header("Location: results.php");
}
include("../race/system/functions.php");
include('header.php');

$roundNo = $_GET['roundNo'];

$data = getRaceResults();
$data = $data[$roundNo];


$raceArray = array();

foreach($data as $RaceNoKey => $race){

    $className = ($race[0]['RoundType'] == "M" ? rtrim($race[0]['Class']) . " - " . rtrim($race[0]['Heat']) . " Main" : rtrim($race[0]['Class']));

    foreach($race as &$driver){

        //Clean trailing spaces
        foreach($driver as &$driverData){
            $driverData = rtrim($driverData);
        }

        $driver['RaceTime'] = formatSeconds($driver['RaceTime']);
        $driver['FastLap'] = ($driver['FastLap'] == '999' ? "" : strval(round($driver['FastLap'], 3)));
    }

    $newArray = array(
        "RaceNo" 	=> $RaceNoKey,
        "Class"		=> $className,
        "Drivers"	=> $race
    );

    array_push($raceArray, $newArray);
}
?>

<script>
    $(document).ready(function(){
        $("#navbar .results").addClass("ui-btn-active");
    });
</script>
<h1>Results For Round <?=$roundNo?></h1>
<ul data-role="listview" data-inset="true">
    <?foreach($raceArray as $race){
        echo "<li data-role=\"list-divider\">Race ".$race['RaceNo']." - ".$race['Class']."</li>";

        foreach($race['Drivers'] as $driver){
            ?>
              <li>
                  <a href="results-race-driverdata.php?<?=http_build_query($driver, "&amp;")?>">
                  <h3><?=$driver['FullName']?></h3>
                  <p>
                      <b>Position:</b> <?=$driver['Position'];?> |
                      <b>Race Time :</b> <?=$driver['RaceTime'];?> |
                      <b>Fast Lap:</b> <?=$driver['FastLap'];?>
                  </p>
                  <p class="ui-li-aside">
                  <b>Laps: </b> <?=$driver['Laps']?><br>
                  <b>Qualifying Position: </b> <?=$driver['OverallQualPos']?><br>
                  </p>
                  </a>
              </li>

            <?
        }

    }?>


</ul>
