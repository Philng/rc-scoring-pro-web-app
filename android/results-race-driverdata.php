<?php
/**
 * User: Phil Nguyen
 * Date: 1/22/14
 * Time: 2:12 PM
 */

include("../race/system/functions.php");
include('header.php');


$paceInfo = getPaceInfo($_GET['DriverID'], $_GET['Round'], $_GET['RaceNo']);


$LapTimeJSON = array();
$paceJSON = array();
foreach($paceInfo as $pace){
    array_push($LapTimeJSON, array($pace['Laps'], $pace['Difference']));
    array_push($paceJSON, array($pace['Laps'], $pace['Pace']));
}


$returnArray = array("LapTime" => $LapTimeJSON, "Pace" => $paceJSON);

?>

<script>
    $(document).ready(function(){
        $("#navbar .results").addClass("ui-btn-active");
    });
</script>
<h1><?=$_GET['Class']?>, Round <?=$_GET['Round']?>, Race <?=$_GET['RaceNo']?></h1>
<h2><?=$_GET['FullName']?></h2>

<div class="ui-grid-a">
    <div class="ui-block-a"><div class="ui-body">
            <table style="">
                <tr><td style="padding-right: 20px;"><b>Position:</b></td><td><?=$_GET['Position']?></td></tr>
                <tr><td style=""><b>Laps:</b></td><td><?=$_GET['Laps']?></td></tr>
                <tr><td style=""><b>Time:</b></td><td><?=$_GET['RaceTime']?></td></tr>
            </table><br>
    </div></div>
    <div class="ui-block-b"><div class="ui-body">
            <table style="">
                <tr><td style="padding-right: 40px;"><b>Fast Lap:</b></td><td><?=(isset($_GET['FastLap']) ? $_GET['FastLap'] : "")?></td></tr>
                <tr><td style=""><b>Avg Top 5:</b></td><td><?=(isset($_GET['AvgTop5']) ? $_GET['AvgTop5'] : "")?></td></tr>
                <tr><td style=""><b>Avg Top 10:</b></td><td><?=(isset($_GET['AvgTop10']) ? $_GET['AvgTop10'] : "")?></td></tr>
            </table><br>
    </div></div>
</div><!-- /grid-a -->

<ol data-role="listview">
    <?for($i = 0; $i < count($LapTimeJSON); $i++){?>
        <li>
            <b>Pace: </b> <?=$paceJSON[$i][1]?>
            <span class="ui-li-aside"><b>Lap Time: </b> <?=$LapTimeJSON[$i][1]?></span>

        </li>

    <?}?>

</ol>